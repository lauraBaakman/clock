class ChangeStartEndColumnNameToStartMomentEndMoment < ActiveRecord::Migration[5.2]
  def change
    rename_column(:time_workeds, :start, :start_moment)
    rename_column(:time_workeds, :end, :end_moment)
  end
end
