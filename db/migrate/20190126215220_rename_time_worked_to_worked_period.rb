class RenameTimeWorkedToWorkedPeriod < ActiveRecord::Migration[5.2]
  def change
    rename_table(:time_workeds, :worked_periods)
  end
end
