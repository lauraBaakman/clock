class StartMomentMayNotBeNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :worked_periods, :started_at, false
  end
end
