# frozen_string_literal: true

#:nodoc:
class RenameStartEndMomentColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column(:worked_periods, :start_moment, :started_at)
    rename_column(:worked_periods, :end_moment, :ended_at)
  end
end
