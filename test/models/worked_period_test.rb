# frozen_string_literal: true

require 'test_helper'

class WorkedPeriodTest < ActiveSupport::TestCase
  attr_reader(:today)

  def setup
    @today = Date.new(2019, 1, 22)
    travel_to @today
  end

  def yesterday
    today - 1.day
  end

  def tomorrow
    today + 1.day
  end

  test 'should validate period where start and end moment are present' do
    worked_period = build(:completed_worked_period)
    assert worked_period.valid?
  end

  test 'should validate period where only start moment is present' do
    worked_period = build(:active_worked_period)
    assert worked_period.valid?
  end

  test 'should invalidate period without start moment' do
    worked_period = build(:worked_period, started_at: nil)
    refute worked_period.valid?
  end

  test 'should validate intersecting active period' do
    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 7, min: 43),
      ended_at: today.to_timewithzone(hour: 10, min: 54)
    )

    today_around_noon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11, min: 20),
      ended_at: today.to_timewithzone(hour: 16, min: 2)
    )

    worked_period = build(
      :worked_period, :active,
      started_at: today.to_timewithzone(hour: 8, min: 30)
    )

    assert worked_period.valid?
  end

  test 'should invalidate intersecting inactive period' do
    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 7, min: 43),
      ended_at: today.to_timewithzone(hour: 10, min: 54)
    )

    today_around_noon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11, min: 20),
      ended_at: today.to_timewithzone(hour: 16, min: 2)
    )

    worked_period = build(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 30),
      ended_at: today.to_timewithzone(hour: 11, min: 0)
    )

    refute worked_period.valid?
  end

  test 'should invalidate too short period' do
    worked_period = build(
      :worked_period,
      started_at: today.to_timewithzone(hour: 10, min: 55),
      ended_at: today.to_timewithzone(hour: 10, min: 56)
    )
    refute worked_period.valid?
  end

  test 'should find no periods on a date when no worked period occured' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 7, min: 55),
      ended_at: yesterday.to_timewithzone(hour: 16, min: 56)
    )

    expected = []

    actual = WorkedPeriod.on(today)

    assert_equal expected, actual
  end

  test 'should find periods on on a date when worked periods occured' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 7, min: 55),
      ended_at: yesterday.to_timewithzone(hour: 16, min: 56)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 7, min: 55),
      ended_at: today.to_timewithzone(hour: 11, min: 33)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 12, min: 28),
      ended_at: today.to_timewithzone(hour: 17, min: 47)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 7, min: 38),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 47)
    )

    expected = [
      today_morning, today_afternoon
    ]

    actual = WorkedPeriod.on(today)

    assert_equal expected, actual
  end

  test 'should find today periods on a date when no worked periods occured' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 7, min: 55),
      ended_at: yesterday.to_timewithzone(hour: 16, min: 56)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 7, min: 48),
      ended_at: tomorrow.to_timewithzone(hour: 17, min: 8)
    )

    expected = []

    actual = WorkedPeriod.today

    assert_equal expected, actual
  end

  test 'should find today periods on a date when worked periods occured' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 7, min: 55),
      ended_at: yesterday.to_timewithzone(hour: 16, min: 56)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 7, min: 55),
      ended_at: today.to_timewithzone(hour: 11, min: 33)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 12, min: 28),
      ended_at: today.to_timewithzone(hour: 17, min: 47)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 7, min: 48),
      ended_at: tomorrow.to_timewithzone(hour: 17, min: 8)
    )

    expected = [today_morning, today_afternoon]

    actual = WorkedPeriod.today

    assert_equal expected, actual
  end

  test 'should find active periods with one active period' do
    active_period = create(:active_worked_period)
    completed_period = create(:completed_worked_period)

    expected = [active_period]

    actual = WorkedPeriod.active

    assert_equal expected, actual
  end

  test 'should find active periods if no active periods' do
    today_completed = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 3),
      ended_at: today.to_timewithzone(hour: 16, min: 59)
    )

    expected = []

    actual = WorkedPeriod.active

    assert_equal expected, actual
  end

  test 'should find worked periods in a range' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 9, min: 3),
      ended_at: yesterday.to_timewithzone(hour: 17, min: 30)
    )

    today_early_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 6, min: 3),
      ended_at: today.to_timewithzone(hour: 8, min: 30)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 9),
      ended_at: today.to_timewithzone(hour: 11, min: 5)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 12, min: 10),
      ended_at: today.to_timewithzone(hour: 17, min: 55)
    )

    today_evening = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 19, min: 33),
      ended_at: today.to_timewithzone(hour: 21, min: 5)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 53),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 43)
    )

    expected = [
      today_afternoon
    ]

    actual = WorkedPeriod.started_in_open_range(
      today.to_timewithzone(hour: 10, min: 10),
      today.to_timewithzone(hour: 16, min: 55)
    )

    assert_equal expected, actual
  end

  test 'should find worked periods in a range delineated by youngest and oldest' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 9, min: 3),
      ended_at: yesterday.to_timewithzone(hour: 17, min: 30)
    )

    today_full_day = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 6, min: 3),
      ended_at: today.to_timewithzone(hour: 14, min: 30)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 53),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 43)
    )

    expected = WorkedPeriod.all

    actual = WorkedPeriod.started_in_open_range(
      WorkedPeriod.oldest.date.beginning_of_day,
      WorkedPeriod.youngest.date.end_of_day
    )
    assert_equal expected.to_a, actual.to_a
  end

  test 'should find worked periods in a range with active period' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_active = create(
      :worked_period, :active,
      started_at: today.to_timewithzone(hour: 11, min: 4)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = [today_morning, today_active]

    actual = WorkedPeriod.started_in_open_range(
      today.to_timewithzone(hour: 8, min: 0),
      today.to_timewithzone(hour: 21, min: 0)
    )

    assert_equal expected, actual
  end

  test 'should find empty relation if no worked periods in a range' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = []

    actual = WorkedPeriod.started_in_open_range(
      today.to_timewithzone(hour: 11, min: 0),
      today.to_timewithzone(hour: 17, min: 0)
    )

    assert_equal expected, actual
  end

  test 'should find worked periods started before or on moment' do
    moment = today.to_timewithzone(hour: 12)

    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period, :active,
      started_at: moment,
      ended_at: today.to_timewithzone(hour: 17, min: 33)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = [
      yesterday_full_day, today_morning, today_afternoon
    ]

    actual = WorkedPeriod.started_before_or_on(
      moment
    )

    assert_equal expected, actual
  end

  test 'should find worked periods before or on moment with active period' do
    moment = today.to_timewithzone(hour: 12)

    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_active = create(
      :worked_period, :active,
      started_at: moment
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = [
      yesterday_full_day, today_morning, today_active
    ]

    actual = WorkedPeriod.started_before_or_on(
      moment
    )

    assert_equal expected, actual
  end

  test 'should find empty relation if no worked periods before or on moment' do
    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 19, min: 33),
      ended_at: tomorrow.to_timewithzone(hour: 21, min: 5)
    )

    expected = []

    actual = WorkedPeriod.started_before_or_on(
      today.to_timewithzone(hour: 9)
    )

    assert_equal expected, actual
  end

  test 'should find worked periods after or on moment with active period' do
    moment = today.to_timewithzone(hour: 12)

    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period, :active,
      started_at: moment,
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = [today_afternoon, tomorrow_full_day]

    actual = WorkedPeriod.started_after_or_on(moment)

    assert_equal expected, actual
  end

  test 'should find worked periods after or on moment' do
    moment = today.to_timewithzone(hour: 12)

    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period,
      started_at: moment,
      ended_at: today.to_timewithzone(hour: 17, min: 33)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = [today_afternoon, tomorrow_full_day]

    actual = WorkedPeriod.started_after_or_on(moment)

    assert_equal expected, actual
  end

  test 'should find empty relation if no worked periods after or on moment' do
    moment = today.to_timewithzone(hour: 12)

    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    expected = []

    actual = WorkedPeriod.started_after_or_on(moment)

    assert_equal expected, actual
  end

  test 'should return period date' do
    worked_period = build(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8)
    )

    actual = worked_period.date

    expected = today

    assert_equal expected, actual
  end

  test 'should find active_period' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_active = create(
      :worked_period, :active,
      started_at: today.to_timewithzone(hour: 11)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = today_active

    actual = WorkedPeriod.active_period

    assert_equal expected, actual
  end

  test 'should find nill if no active_period' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11),
      ended_at: today.to_timewithzone(hour: 17)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    actual = WorkedPeriod.active_period

    assert_nil actual
  end

  test 'should find oldest period' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11),
      ended_at: today.to_timewithzone(hour: 17)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = yesterday_full_day

    actual = WorkedPeriod.oldest

    assert_equal expected, actual
  end

  test 'should find youngest period' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11),
      ended_at: today.to_timewithzone(hour: 17)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    expected = tomorrow_full_day

    actual = WorkedPeriod.youngest

    assert_equal expected, actual
  end

  test 'should indicate that no active period exists' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_afternoon = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 11),
      ended_at: today.to_timewithzone(hour: 17)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    refute WorkedPeriod.active_period?
  end

  test 'should indicate that an active period exists' do
    yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 19, min: 33),
      ended_at: yesterday.to_timewithzone(hour: 21, min: 5)
    )

    today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 8, min: 53),
      ended_at: today.to_timewithzone(hour: 10, min: 43)
    )

    today_active = create(
      :worked_period, :active,
      started_at: today.to_timewithzone(hour: 11)
    )

    tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 8, min: 21),
      ended_at: tomorrow.to_timewithzone(hour: 16, min: 17)
    )

    assert WorkedPeriod.active_period?
  end
end
