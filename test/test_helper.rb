# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/pride'

module CoreExtensions
  #:nodoc:
  module Date
    module Conversions
      def to_timewithzone(zone = ::Time.zone, hour: 0, min: 0, sec: 0)
        validate = lambda do |description, range, value|
          return if range.cover?(value)

          error_msg = '%s should be in %s, got %d.'
          msg = format(error_msg, description.capitalize, range, value)
          raise ArgumentError, msg
        end

        validate.call('hours', 0..23, hour)
        validate.call('minutes', 0..59, min)
        validate.call('seconds', 0..59, sec)

        time_off_set = (
          hour * 1.hours +
          min * 1.minutes +
          sec * 1.seconds
        )
        datetime = to_datetime + time_off_set

        zone.parse(datetime.to_s(:db))
      end
    end
  end
end

Date.include CoreExtensions::Date::Conversions

module ActiveSupport
  class TestCase
    ActiveRecord::Migration.check_pending!

    include FactoryBot::Syntax::Methods

    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.logger = Rails.logger

    setup { DatabaseCleaner.start }
    teardown { DatabaseCleaner.clean }
  end
end
