# frozen_string_literal: true

class DummyTimerAble
  include Timer

  def initialize(started_at, ended_at = nil)
    @started_at = started_at
    @ended_at = ended_at
  end

  attr_accessor :started_at
  attr_accessor :ended_at
end

class TimerAbleTest < ActiveSupport::TestCase
  attr_reader(:started_at, :ended_at, :now, :active_timer, :inactive_timer)

  def setup
    @started_at = Time.zone.local(2018, 10, 15, 9, 5, 0)
    @ended_at = Time.zone.local(2018, 10, 15, 17, 56, 39)
    @now = Time.zone.local(2018, 10, 15, 19, 23, 18)

    @active_timer = DummyTimerAble.new(@started_at)
    @inactive_timer = DummyTimerAble.new(@started_at, @ended_at)

    travel_to @now
  end

  test 'should give duration of active timer' do
    expected = 10.hours + 18.minutes + 18.seconds

    actual = active_timer.duration

    # float == duration if float == duration.seconds
    assert_instance_of ActiveSupport::Duration, actual
    assert_equal expected, actual
  end

  test 'should give duration of inactive timer' do
    expected = 8.hours + 51.minutes + 39.seconds
    actual = inactive_timer.duration

    # float == duration if float == duration.seconds
    assert_instance_of ActiveSupport::Duration, actual
    assert_equal expected, actual
  end

  test 'should indicate that the timer is active' do
    assert active_timer.active?
    assert_not active_timer.inactive?
  end

  test 'should indicate that the timer is inactive' do
    assert_not inactive_timer.active?
    assert inactive_timer.inactive?
  end

  test 'should give the range of an inactive timer inc start and inc end' do
    expected = started_at..ended_at

    actual = inactive_timer.range

    assert_equal expected, actual
  end

  test 'should give the range of an active timer inc start and inc end' do
    expected = started_at..now

    actual = active_timer.range

    assert_equal expected, actual
  end

  test 'should give the range of an inactive timer inc start ex end' do
    expected = started_at...ended_at

    actual = inactive_timer.range(exclude_end: true)

    assert_equal expected, actual
  end

  test 'should give the range of an active timer inc start ex end' do
    expected = started_at...now

    actual = active_timer.range(exclude_end: true)
    assert_equal expected, actual
  end
end
