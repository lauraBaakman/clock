# frozen_string_literal: true

require 'test_helper'

class WorkedPeriodValidatorTest < ActiveSupport::TestCase
  attr_reader(:now)
  def setup
    @now = Time.zone.local(2019, 1, 23, 15, 0, 0)
  end

  def today
    now.to_date
  end

  def yesterday
    today.yesterday
  end

  def create_active_period
    create(
      :worked_period, :active,
      started_at: now
    )
  end

  test 'valid end absent no active period' do
    worked_period = build(
      :worked_period,
      started_at: now
    )

    assert worked_period.valid?

    worked_period.save!

    assert worked_period.valid?
  end

  test 'valid end present no active period' do
    worked_period = build(
      :worked_period,
      started_at: now,
      ended_at: now + 8.hours
    )

    assert worked_period.valid?

    worked_period.save!

    assert worked_period.valid?
  end

  test 'valid end present with active period' do
    create_active_period

    worked_period = build(
      :worked_period,
      started_at: now + 2.hours,
      ended_at: now + 8.hours
    )

    assert worked_period.valid?

    worked_period.save!

    assert worked_period.valid?
  end

  test 'valid self is active period' do
    active_period = create_active_period

    assert active_period.valid?
  end

  test 'invalid end absent with active period' do
    create_active_period

    work_period = build(
      :worked_period, :active,
      started_at: now + 2.hours
    )

    refute work_period.valid?
    assert_includes(
      work_period.errors[:base],
      'A work period is already in progress.'
    )
  end

  test 'invalid end date before start date' do
    work_period = build(
      :worked_period,
      started_at: now + 8.hours,
      ended_at: now + 2.hours
    )

    refute work_period.valid?
    assert_includes(
      work_period.errors[:base],
      'The end of a work period should be after its start.'
    )
  end

  test 'invalid start date and end date not on same day' do
    work_period = build(
      :worked_period,
      started_at: yesterday,
      ended_at: now
    )

    refute work_period.valid?
    assert_includes(
      work_period.errors[:base],
      'A work period should start and end on the same day.'
    )
  end

  test 'invalid start moment and end moment are the same' do
    work_period = build(
      :worked_period,
      started_at: now,
      ended_at: now
    )

    refute work_period.valid?
    assert_includes(
      work_period.errors[:base],
      'The work period should be longer than 5 minutes.'
    )
  end

  test 'too short period' do
    work_period = build(
      :worked_period,
      started_at: now,
      ended_at: now + 2.seconds
    )

    refute work_period.valid?
    assert_includes(
      work_period.errors[:base],
      'The work period should be longer than 5 minutes.'
    )
  end
end
