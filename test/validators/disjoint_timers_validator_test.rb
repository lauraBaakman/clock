# frozen_string_literal: true

class DisjointTimerValidatorTest < ActiveSupport::TestCase
  def setup
    create_mock_table

    @mock_class = build_mock_class

    fill_mock_table

    @now = @active_timer.started_at + 1.hour + 5.minutes

    @intersection_error = 'This timer intersects with at least one existing '\
                          'timer.'
  end

  def teardown
    drop_mock_table
  end

  def create_mock_table
    ActiveRecord::Base.connection.create_table :mock_table do |t|
      t.datetime :started_at
      t.datetime :ended_at
    end
  end

  def build_mock_class
    Class.new(ActiveRecord::Base) do
      self.table_name = 'mock_table'
      reset_column_information

      validates_with DisjointTimersValidator

      include Timer

      scope :exclude, ->(record) { where.not(id: record.id) }
    end
  end

  def fill_mock_table
    @inactive_timer_past0 = @mock_class.create(
      started_at: Time.zone.local(2019, 1, 4, 8, 30, 17),
      ended_at: Time.zone.local(2019, 1, 4, 10, 29, 10)
    )

    @inactive_timer_past1 = @mock_class.create(
      started_at: Time.zone.local(2019, 1, 5, 9, 0, 18),
      ended_at: Time.zone.local(2019, 1, 5, 11, 0, 9)
    )

    @active_timer = @mock_class.create(
      started_at: Time.zone.local(2019, 1, 6, 8, 0, 26)
    )

    @inactive_timer_future2 = @mock_class.create(
      started_at: Time.zone.local(2019, 1, 7, 10, 1, 19),
      ended_at: Time.zone.local(2019, 1, 7, 13, 2, 10)
    )
  end

  def drop_mock_table
    ActiveRecord::Base.connection.drop_table :mock_table
  end

  test 'should validate a disjoint inactive timer' do
    new_instance = @mock_class.new(
      started_at: Time.zone.local(2019, 1, 7, 7, 30, 19),
      ended_at: Time.zone.local(2019, 1, 7, 9, 38, 53)
    )

    assert new_instance.valid?
  end

  test 'should validate an inactive timer between existing timers' do
    new_instance = @mock_class.new(
      started_at: Time.zone.local(2019, 1, 5, 13, 33, 42),
      ended_at: Time.zone.local(2019, 1, 6, 5, 8, 37)
    )

    assert new_instance.valid?
  end

  test 'should validate an inactive timer that starts at existing end' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past1.ended_at,
      ended_at: @inactive_timer_past1.ended_at + 30.minutes
    )

    assert new_instance.valid?
  end

  test 'should validate a timer exactly between two existing timers' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.ended_at,
      ended_at: @inactive_timer_past1.started_at
    )

    assert new_instance.valid?
  end

  test 'should validate an inactive timer that ends at existing start' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past1.started_at - 2.hours,
      ended_at: @inactive_timer_past1.started_at
    )

    assert new_instance.valid?
  end

  test 'should validate disjoint zero duration' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.ended_at + 10.minutes,
      ended_at: @inactive_timer_past1.started_at - 1.hours
    )

    assert new_instance.valid?
  end

  test 'should validate active timer that is already in db' do
    assert @active_timer.valid?
  end

  test 'should validate active timer that is not in db' do
    @active_timer.destroy
    assert @active_timer.valid?
  end

  test 'should validate inactive timer that is already in db' do
    assert @inactive_timer_past0.valid?
  end

  test 'should validate inactive timer that is after an active timer' do
    assert @inactive_timer_future2.valid?
  end

  test 'should invalidate inactive timer that starts at existing start' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past1.started_at,
      ended_at: @inactive_timer_past1.ended_at + 30.minutes
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end

  test 'should invalidate inactive timer within existing timer' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past1.started_at + 10.minutes,
      ended_at: @inactive_timer_past1.started_at + 30.minutes
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end

  test 'should invalidate inactive timer that ends on existing end' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past1.started_at - 10.minutes,
      ended_at: @inactive_timer_past1.ended_at
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end

  test 'should invalidate inactive timer that starts and ends in disjoint '\
       'inactive timers' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.started_at + 10.minutes,
      ended_at: @inactive_timer_past1.ended_at - 5.minutes
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end

  test 'should invalidate inactive timer that supersets inactive timers' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.started_at - 10.minutes,
      ended_at: @inactive_timer_past1.ended_at + 5.minutes
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end

  test 'should validate inactive timer that supersets active timer' do
    new_instance = @mock_class.new(
      started_at: @active_timer.started_at - 10.minutes,
      ended_at: @now + 1.hour
    )

    assert new_instance.valid?
  end

  test 'should validate inactive timer that starts at active timer start' do
    new_instance = @mock_class.new(
      started_at: @active_timer.started_at,
      ended_at: @now + 2.hours
    )

    assert new_instance.valid?
  end

  test 'should validate inactive timer within active timer' do
    new_instance = @mock_class.new(
      started_at: @active_timer.started_at + 10.minutes,
      ended_at: @active_timer.started_at + 15.minutes
    )

    assert new_instance.valid?
  end

  test 'should validate inactive timer that ends on now' do
    new_instance = @mock_class.new(
      started_at: @active_timer.started_at - 10.minutes,
      ended_at: @now
    )

    assert new_instance.valid?
  end

  test 'should validate zero duration timer at existing start' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.started_at,
      ended_at: @inactive_timer_past0.started_at
    )

    assert new_instance.valid?
  end

  test 'should validate zero duration timer at existing end' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.ended_at,
      ended_at: @inactive_timer_past0.ended_at
    )

    assert new_instance.valid?
  end

  test 'should invalidate zero duration timer within existing timer' do
    new_instance = @mock_class.new(
      started_at: @inactive_timer_past0.started_at + 10.minutes,
      ended_at: @inactive_timer_past0.started_at + 10.minutes
    )

    refute new_instance.valid?
    assert_includes new_instance.errors[:base], @intersection_error
  end
end
