# frozen_string_literal: true

FactoryBot.define do
  factory :worked_period, aliases: [:active_worked_period] do
    started_at { '2019-03-21 08:12:26' }

    trait :active do
      ended_at { nil }
    end

    factory :completed_worked_period do
      started_at { '2019-03-22 07:55:18' }
      ended_at { '2019-03-22 16:46:23' }
    end
  end
end
