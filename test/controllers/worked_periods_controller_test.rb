# frozen_string_literal: true

require 'test_helper'

class WorkedPeriodsControllerTest < ActionDispatch::IntegrationTest
  attr_reader(
    :now, :worked_period,
    :yesterday_full_day, :yesterday_minus_one_full_day,
    :today_morning, :today_afternoon,
    :tomorrow_full_day
  )

  setup do
    @now = Time.zone.local(2019, 1, 22, 15, 0, 0)
    create_periods

    @worked_period = yesterday_full_day

    travel_to @now
  end

  def create_periods
    @tomorrow_full_day = create(
      :worked_period,
      started_at: tomorrow.to_timewithzone(hour: 7),
      ended_at: tomorrow.to_timewithzone(hour: 18)
    )

    @today_morning = create(
      :worked_period,
      started_at: today.to_timewithzone(hour: 7),
      ended_at: today.to_timewithzone(hour: 11, min: 30)
    )
    @today_afternoon = create(
      :worked_period, :active,
      started_at: today.to_timewithzone(hour: 12, min: 15)
    )

    @yesterday_full_day = create(
      :worked_period,
      started_at: yesterday.to_timewithzone(hour: 7, min: 45),
      ended_at: yesterday.to_timewithzone(hour: 16)
    )
    @yesterday_minus_one_full_day = create(
      :worked_period,
      started_at: (yesterday - 1).to_timewithzone(hour: 9, min: 15),
      ended_at: (yesterday - 1).to_timewithzone(hour: 17, min: 15)
    )
  end

  def today
    @now.to_date
  end

  def yesterday
    today - 1
  end

  def tomorrow
    today + 1
  end

  test 'should get index' do
    expected_worked_periods = [
      yesterday_minus_one_full_day,
      yesterday_full_day,
      today_morning,
      today_afternoon,
      tomorrow_full_day
    ]

    expected_worked_time = 34.hours + 30.minutes

    get worked_periods_url

    assert_response :success

    assert_equal(expected_worked_periods, assigns[:worked_periods].to_a)
    assert_equal(expected_worked_time, assigns[:worked_time])
    assert_nil(assigns[:worked_period])
  end

  test 'should get index filtered if started_at and ended_at are dates' do
    expected_worked_periods = [
      yesterday_full_day,
      today_morning,
      today_afternoon
    ]
    expected_worked_time = 15.hours + 30.minutes

    get(
      worked_periods_url,
      params: {
        started_on: yesterday,
        ended_on: today
      }
    )

    assert_response :success

    assert_equal(expected_worked_periods, assigns[:worked_periods])
    assert_equal(expected_worked_time, assigns[:worked_time])
    assert_nil(assigns[:worked_period])
  end

  test 'should get index filtered if started_at and ended_at are datetimes' do
    expected_worked_periods = [
      yesterday_full_day,
      today_morning,
      today_afternoon
    ]
    expected_worked_time = 15.hours + 30.minutes

    get(
      worked_periods_url,
      params: {
        started_on: yesterday.to_timewithzone(hour: 8),
        ended_on: today.to_timewithzone(hour: 8)
      }
    )

    assert_response :success

    assert_equal(expected_worked_periods, assigns[:worked_periods])
    assert_equal(expected_worked_time, assigns[:worked_time])
    assert_nil(assigns[:worked_period])
  end

  test 'should get index filtered if ended_at is nil' do
    expected_worked_periods = [
      yesterday_full_day,
      today_morning,
      today_afternoon,
      tomorrow_full_day
    ]
    expected_worked_time = 26.hours + 30.minutes

    get(
      worked_periods_url,
      params: {
        started_on: yesterday
      }
    )

    assert_response :success

    assert_equal(expected_worked_periods, assigns[:worked_periods])
    assert_equal(expected_worked_time, assigns[:worked_time])
    assert_nil(assigns[:worked_period])
  end

  test 'should get index filtered if started_at is nil' do
    expected_worked_periods = [
      yesterday_minus_one_full_day,
      yesterday_full_day,
      today_morning,
      today_afternoon
    ]
    expected_worked_time = 23.hours + 30.minutes

    get(
      worked_periods_url,
      params: {
        ended_on: today
      }
    )

    assert_response :success

    assert_equal(expected_worked_periods, assigns[:worked_periods])
    assert_equal(expected_worked_time, assigns[:worked_time])
    assert_nil(assigns[:worked_period])
  end

  test 'should get new' do
    get new_worked_period_url

    assert_response :success

    refute_nil(assigns[:worked_period])
    assert_nil(assigns[:worked_periods])
    assert_nil(assigns[:worked_time])
  end

  test 'should create worked_period' do
    assert_difference('WorkedPeriod.count') do
      post worked_periods_url, params: {
        worked_period: {
          started_at: (tomorrow + 1).to_timewithzone(hour: 8),
          ended_at: (tomorrow + 1).to_timewithzone(hour: 16, min: 30)
        }
      }
    end

    assert_redirected_to worked_period_url(WorkedPeriod.last)

    refute_nil(assigns[:worked_period])
    assert_nil(assigns[:worked_periods])
    assert_nil(assigns[:worked_time])
  end

  test 'should show worked_period' do
    get worked_period_url(worked_period)
    assert_response :success

    refute_nil(assigns[:worked_period])
    assert_nil(assigns[:worked_periods])
    assert_nil(assigns[:worked_time])
  end

  test 'should get edit' do
    get edit_worked_period_url(worked_period)
    assert_response :success
  end

  test 'should update worked_period' do
    new_started_at = worked_period.date.to_timewithzone(hour: 8)
    new_ended_at = worked_period.date.to_timewithzone(hour: 16, min: 15)

    patch(
      worked_period_url(worked_period),
      params: {
        worked_period: {
          started_at: new_started_at,
          ended_at: new_ended_at
        }
      }
    )
    assert_redirected_to worked_period_url(worked_period)

    assert_nil(assigns[:worked_time])
    assert_nil(assigns[:worked_periods])
    refute_nil(assigns[:worked_period])
    assert(new_started_at, assigns[:worked_period].started_at)
    assert(new_ended_at, assigns[:worked_period].ended_at)
  end

  test 'should destroy worked_period' do
    assert_difference('WorkedPeriod.count', -1) do
      delete worked_period_url(worked_period)
    end

    assert_redirected_to worked_periods_url

    assert_equal(worked_period, assigns[:worked_period])
    assert_nil(assigns[:worked_time])
    assert_nil(assigns[:worked_periods])
  end
end
