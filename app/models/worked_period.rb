# frozen_string_literal: true

# == Schema Information
#
# Table name: worked_periods
#
#  id         :bigint(8)        not null, primary key
#  ended_at   :datetime
#  started_at :datetime         not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

#
# Time spent working.
#
# @author [laura]
class WorkedPeriod < ApplicationRecord
  validates :started_at, presence: true
  validates :ended_at, presence: false
  validate :instance_validations

  include Timer

  scope :exclude, ->(record) { where.not(id: record.id) }

  scope :on, ->(date) { where('started_at': date.all_day) }

  scope :started_before_or_on, lambda { |moment|
    where('started_at <= :moment', moment: moment)
  }

  scope :started_after_or_on, lambda { |moment|
    where('started_at >= :moment', moment: moment)
  }

  scope :started_in_open_range, lambda { |inclusive_start, inclusive_end|
    started_before_or_on(
      inclusive_end
    ).started_after_or_on(
      inclusive_start
    )
  }

  scope :today, -> { on(Date.current) }

  scope :active, -> { where(ended_at: nil) }

  def self.active_period
    find_by(ended_at: nil)
  end

  def self.active_period?
    active_period.present?
  end

  def self.oldest
    WorkedPeriod.order(started_at: :asc).first
  end

  def self.youngest
    WorkedPeriod.order(started_at: :desc).first
  end

  def date
    started_at.to_date
  end

  def instance_validations
    validates_with WorkedPeriodValidator, DisjointTimersValidator
  end
end
