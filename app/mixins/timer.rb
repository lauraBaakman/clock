# frozen_string_literal: true

#
# Mixin to create a single use timer.
# Requires the read and write access of a started_at and ended_at.
#
# @author [laura]
#
module Timer
  def active?
    ended_at.nil?
  end

  def inactive?
    !active?
  end

  def duration
    seconds = current_ended_at - started_at
    ActiveSupport::Duration.build(seconds)
  end

  def range(exclude_end: false)
    Range.new(started_at, current_ended_at, exclude_end)
  end

  def current_ended_at
    ended_at || Time.current
  end
end
