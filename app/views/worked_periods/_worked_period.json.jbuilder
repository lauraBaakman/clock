json.extract! time_worked, :id, :started_at, :ended_at, :created_at, :updated_at
json.url time_worked_url(time_worked, format: :json)
