# frozen_string_literal: true

#:nodoc:
class WorkedPeriodsController < ApplicationController
  before_action :set_worked_period, only: %i[show edit update destroy]

  # GET /worked_periods
  # GET /worked_periods.json
  def index
    started_on = params.fetch(:started_on, WorkedPeriod.oldest.date)
    started_on = started_on.to_date.beginning_of_day

    ended_on = params.fetch(:ended_on, WorkedPeriod.youngest.date)
    ended_on = ended_on.to_date.end_of_day

    @worked_periods = WorkedPeriod.started_in_open_range(
      started_on, ended_on
    ).order(
      started_at: :asc
    )
    @worked_time = @worked_periods.to_a.sum(&:duration)
  end

  # GET /worked_periods/1
  # GET /worked_periods/1.json
  def show; end

  # GET /worked_periods/new
  def new
    @worked_period = WorkedPeriod.new
  end

  # GET /worked_periods/1/edit
  def edit; end

  # POST /worked_periods
  # POST /worked_periods.json
  def create
    @worked_period = WorkedPeriod.new(worked_period_params)

    respond_to do |format|
      if @worked_period.save
        format.html do
          redirect_to(
            @worked_period, notice: 'Time worked was successfully created.'
          )
        end
        format.json { render :show, status: :created, location: @worked_period }
      else
        format.html { render :new }
        format.json do
          render(json: @worked_period.errors, status: :unprocessable_entity)
        end
      end
    end
  end

  # PATCH/PUT /worked_periods/1
  # PATCH/PUT /worked_periods/1.json
  def update
    respond_to do |format|
      if @worked_period.update(worked_period_params)
        format.html do
          redirect_to(
            @worked_period, notice: 'Time worked was successfully updated.'
          )
        end
        format.json { render :show, status: :ok, location: @worked_period }
      else
        format.html { render :edit }
        format.json do
          render json: @worked_period.errors, status: :unprocessable_entity
        end
      end
    end
  end

  # DELETE /worked_periods/1
  # DELETE /worked_periods/1.json
  def destroy
    @worked_period.destroy
    respond_to do |format|
      format.html do
        redirect_to(
          worked_periods_url, notice: 'Time worked was successfully destroyed.'
        )
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_worked_period
    @worked_period = WorkedPeriod.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list.
  def worked_period_params
    params.require(:worked_period).permit(:started_at, :ended_at)
  end
end
