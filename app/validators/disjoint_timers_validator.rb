# frozen_string_literal: true

#
# Validate for Timers that need to be disjoint. I.e. a timer is valid if
# it does not overlap with any other timer in that table. Timers are allowed
# to start on the exact moment another timer ends.
#
# @author [laura]
#
class DisjointTimersValidator < ActiveModel::Validator
  def validate(record)
    return unless intersecting_timers(record).any?

    record.errors.add(
      :base, 'This timer intersects with at least one existing timer.'
    )
  end

  private

  def intersecting_timers(record)
    inactive_timers(record).where(
      'started_at < :record_ended_at', record_ended_at: record.ended_at
    ).where(
      'ended_at > :record_started_at', record_started_at: record.started_at
    ).exclude(
      record
    )
  end

  def inactive_timers(record)
    record.class.where.not(ended_at: nil)
  end
end
