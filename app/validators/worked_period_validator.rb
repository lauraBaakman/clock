# frozen_string_literal: true

#
# Validator for WorkedPeriods. Validate that the worked period does not overlap
# with existing worked periods and that it is a valid period.
#
# @author [laura]
class WorkedPeriodValidator < ActiveModel::Validator
  DEFAULT_MIN_PERIOD_DURATION = 5.minutes

  include ActionView::Helpers::DateHelper

  def validate(record)
    only_one_active_period(record)
    validate_range(record, **options)
  end

  private

  def only_one_active_period(record)
    return unless record.active?

    other_active_periods = record.class.active.exclude(record)
    return unless other_active_periods.any?

    message = 'A work period is already in progress.'
    record.errors[:base] << message
  end

  def validate_range(record, **options)
    return if record.active?

    exceeds_minimum_length(record, **options)
    start_and_end_on_same_date(record)
    end_not_before_start(record)
  end

  def end_not_before_start(record)
    return unless record.ended_at <= record.started_at

    message = 'The end of a work period should be after its start.'
    record.errors[:base] << message
  end

  def start_and_end_on_same_date(record)
    return unless record.started_at.to_date != record.ended_at.to_date

    message = 'A work period should start and end on the same day.'
    record.errors[:base] << message
  end

  def exceeds_minimum_length(record, min_duration: DEFAULT_MIN_PERIOD_DURATION)
    return unless record.duration < min_duration

    message = 'The work period should be longer than '\
              "#{distance_of_time_in_words(min_duration)}."
    record.errors[:base] << message
  end
end
